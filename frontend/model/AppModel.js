export default class AppModel {
    static async getFlights() {
        try {
            const flightsResponse = await fetch('http://localhost:5555/flights');
            const flightsBody = await flightsResponse.json();

            if (flightsResponse.status !== 200) {
                return Promise.reject(flightsBody);
            }

            return {
                flights: flightsBody.flights,
                airplanes: flightsBody.airplanes
            };
        } catch (err) {
            return Promise.reject({
                timestamp: new Date().toISOString(),
                statusCode: 0,
                message: err.message
            });
        }
    }

    // РЕЙСЫ
    static async addFlight({
        flightID,
        departureTimestamp,
        destination,
        airplaneID
    }) {
        try {
            const addFlightResponse = await fetch(
                'http://localhost:5555/flights',
                {
                    method: 'POST',
                    body: JSON.stringify({
                        flightID,
                        departureTimestamp,
                        destination,
                        airplaneID
                    }),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            );

            if (addFlightResponse.status !== 200) {
                const addFlightBody = await addFlightResponse.json();
                return Promise.reject(addFlightBody);
            }

            return {
                timestamp: new Date().toISOString(),
                message: `Рейс ${flightID} успешно создан`
            };
        } catch (err) {
            return Promise.reject({
                timestamp: new Date().toISOString(),
                statusCode: 0,
                message: err.message
            });
        }
    }

    static async editFlight({
        flightID,
        departureTimestamp,
        destination,
        airplaneID
    }) {
        try {
            const editFlightResponse = await fetch(
                `http://localhost:5555/flights/${flightID}`,
                {
                    method: 'PATCH',
                    body: JSON.stringify({
                        departureTimestamp,
                        destination,
                        airplaneID
                    }),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            );

            if (editFlightResponse.status !== 200) {
                const editFlightBody = await editFlightResponse.json();
                return Promise.reject(editFlightBody);
            }

            return {
                timestamp: new Date().toISOString(),
                message: `Рейс ${flightID} успешно изменен`
            };
        } catch (err) {
            return Promise.reject({
                timestamp: new Date().toISOString(),
                statusCode: 0,
                message: err.message
            });
        }
    }

    static async deleteFlight({ flightID }) {
        try {
            const deleteFlightResponse = await fetch(
                `http://localhost:5555/flights/${flightID}`,
                {
                    method: 'DELETE',
                }
            );

            if (deleteFlightResponse.status !== 200) {
                const deleteFlightBody = await deleteFlightResponse.json();
                return Promise.reject(deleteFlightBody);
            }

            return {
                timestamp: new Date().toISOString(),
                message: `Рейс ${flightID} успешно удален`
            };
        } catch (err) {
            return Promise.reject({
                timestamp: new Date().toISOString(),
                statusCode: 0,
                message: err.message
            });
        }
    }

    // БИЛЕТЫ
    static async addTicket({
        ticketID,
        fullName,
        flightID
    }) {
        try {
            const addTicketResponse = await fetch(
                'http://localhost:5555/tickets',
                {
                    method: 'POST',
                    body: JSON.stringify({
                        ticketID,
                        fullName,
                        flightID
                    }),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            );

            if (addTicketResponse.status !== 200) {
                const addTicketBody = await addTicketResponse.json();
                return Promise.reject(addTicketBody);
            }

            return {
                timestamp: new Date().toISOString(),
                message: `Билет ${ticketID} успешно создан`
            };
        } catch (err) {
            return Promise.reject({
                timestamp: new Date().toISOString(),
                statusCode: 0,
                message: err.message
            });
        }
    }

    static async editTicket({
        ticketID,
        fullName,
    }) {
        try {
            const editTicketResponse = await fetch(
                `http://localhost:5555/tickets/${ticketID}`,
                {
                    method: 'PATCH',
                    body: JSON.stringify({
                        fullName
                    }),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            );

            if (editTicketResponse.status !== 200) {
                const editTicketBody = await editTicketResponse.json();
                return Promise.reject(editTicketBody);
            }

            return {
                timestamp: new Date().toISOString(),
                message: `Билет ${ticketID} успешно изменен`
            };
        } catch (err) {
            return Promise.reject({
                timestamp: new Date().toISOString(),
                statusCode: 0,
                message: err.message
            });
        }
    }

    static async deleteTicket({ ticketID }) {
        try {
            const deleteTicketResponse = await fetch(
                `http://localhost:5555/tickets/${ticketID}`,
                {
                    method: 'DELETE',
                }
            );

            if (deleteTicketResponse.status !== 200) {
                const deleteTicketBody = await deleteTicketResponse.json();
                return Promise.reject(deleteTicketBody);
            }

            return {
                timestamp: new Date().toISOString(),
                message: `Билет ${ticketID} успешно удален`
            };
        } catch (err) {
            return Promise.reject({
                timestamp: new Date().toISOString(),
                statusCode: 0,
                message: err.message
            });
        }
    }

    static async moveTicket({
            ticketID,
            srcFlightID,
            destFlightID
        }) {
        try {
            const moveTicketResponse = await fetch(
                `http://localhost:5555/flights`,
                {
                    method: 'PATCH',
                    body: JSON.stringify({
                        ticketID,
                        srcFlightID,
                        destFlightID
                    }),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            );

            if (moveTicketResponse.status !== 200) {
                const moveTicketBody = await moveTicketResponse.json();
                return Promise.reject(moveTicketBody);
            }

            return {
                timestamp: new Date().toISOString(),
                message: `Билет ${ticketID} успешно перемещен`
            };
        } catch (err) {
            return Promise.reject({
                timestamp: new Date().toISOString(),
                statusCode: 0,
                message: err.message
            });
        }
    }
};