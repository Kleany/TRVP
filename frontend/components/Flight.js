export default class Flight {
    #tickets = [];
    #flightID;
    #destination;
    #departureTimestamp;
    #airplaneName;

        constructor({
            flightID,
            destination,
            departureTimestamp,
            airplaneName,
        }) {
        this.#flightID = flightID;
        this.#destination = destination;
        this.#departureTimestamp = departureTimestamp;
        this.#airplaneName = airplaneName;
    }

    get flightID() { return this.#flightID; }

    get tickets() { return this.#tickets; }

    set destination(value) { this.#destination = value; }
    set departureTimestamp(value) { this.#departureTimestamp = value; }
    set airplaneName(value) { this.#airplaneName = value; }

    appendNewTicket(ticketObject) {
        this.#tickets.push(ticketObject);
    }

    popTicket(ticketID) {
        this.#tickets = this.#tickets.filter((ticket) => ticket.ticketID !== ticketID);
    }

    render() {
        // Создание HTML элементов рейса
        const outerDivElement = document.createElement('div');
        outerDivElement.classList.add('flights-list__item-1');

        const liElement = document.createElement('li');
        liElement.classList.add('flights-list__item', 'flight', 'col-auto');
        liElement.setAttribute('id', this.#flightID);

        const divElement1 = document.createElement('div');
        divElement1.classList.add('flights-list__item-info', 'row');

        const divElement2 = document.createElement('div');
        divElement2.classList.add('flights-list__item-info__header', 'row');

        const spanID = document.createElement('span');
        spanID.classList.add('flight__id', 'col-8', 'align-self-end');
        spanID.innerHTML = this.#flightID;
        divElement2.appendChild(spanID);

        const buttonEdit = document.createElement('button');
        buttonEdit.classList.add('flight__edit-btn', 'btn', 'btn-outline-primary', 'col-2');
        buttonEdit.setAttribute('type', 'button');
        buttonEdit.innerHTML = '&#10000;';
        buttonEdit.addEventListener('click', () => {
            document.getElementById('modal-edit-flight').querySelector('.modal-ok-btn')
                .setAttribute('flight_id', this.#flightID);
            document.getElementById('modal-edit-flight').showModal();
        });
        divElement2.appendChild(buttonEdit);

        const buttonDelete = document.createElement('button');
        buttonDelete.classList.add('flight__delete-btn', 'btn', 'btn-outline-danger', 'col-2');
        buttonDelete.setAttribute('type', 'button');
        buttonDelete.innerHTML = '&#10008;';
        buttonDelete.addEventListener('click', () => {
            document.getElementById('modal-delete-flight').querySelector('.modal-ok-btn')
                .setAttribute('flight_id', this.#flightID);
            document.getElementById('modal-delete-flight').showModal();
        });
        divElement2.appendChild(buttonDelete);

        divElement1.appendChild(divElement2);

        const divElement3 = document.createElement('div');
        divElement3.classList.add('flights-list__item-info__main', 'row');

        const innerDiv1 = document.createElement('div');
        innerDiv1.classList.add('col-6');

        const innerSpan1 = document.createElement('span');
        innerSpan1.classList.add('flight__destination', 'row');
        innerSpan1.innerHTML = this.#destination;
        innerDiv1.appendChild(innerSpan1);

        const innerSpan2 = document.createElement('span');
        innerSpan2.classList.add('flight__departure_timestamp', 'row');
        innerSpan2.innerHTML = this.#departureTimestamp;
        innerDiv1.appendChild(innerSpan2);

        divElement3.appendChild(innerDiv1);

        const innerDiv2 = document.createElement('div');
        innerDiv2.classList.add('col-6');

        const innerSpan3 = document.createElement('span');
        innerSpan3.classList.add('flight__airplane_name', 'row');
        innerSpan3.innerHTML = this.#airplaneName;
        innerDiv2.appendChild(innerSpan3);

        divElement3.appendChild(innerDiv2);

        divElement1.appendChild(divElement3);

        outerDivElement.appendChild(divElement1);

        const ulElement = document.createElement('ul');
        ulElement.classList.add('flight__tickets-list');

        // Добавление билетов
        let ticketElement;
        for(let ticket of this.#tickets) {
            ticketElement = ticket.render();
            ulElement.appendChild(ticketElement);
        }

        outerDivElement.appendChild(ulElement);

        const divElement4 = document.createElement('div');
        divElement4.classList.add('row', 'justify-content-center');

        const buttonAddTicket = document.createElement('button');
        buttonAddTicket.classList.add('flight__add-ticket-btn', 'btn', 'btn-outline-primary', 'col-auto');
        buttonAddTicket.setAttribute('type', 'button');
        buttonAddTicket.innerHTML = '&#10010; Добавить билет';
        buttonAddTicket.addEventListener('click', () => {
            document.getElementById('modal-add-ticket').querySelector('.modal-ok-btn')
                .setAttribute('flight_id', this.#flightID);
            document.getElementById('modal-add-ticket').showModal();
        });
        divElement4.appendChild(buttonAddTicket);

        outerDivElement.appendChild(divElement4);

        liElement.appendChild(outerDivElement);
        // конец создания

        return liElement;
    }
};
