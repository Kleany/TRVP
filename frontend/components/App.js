import Flight from "./Flight";
import Ticket from "./Ticket";
import AppModel from "../model/AppModel";

export default class App {
    #flights = [];

    // Функция очистки формы с заданными инпутами
    clearInputs = (ids) => {
        let input;
        for (let id of ids) {
            input = document.getElementById(id);
            input.value = '';
        }
    }

    // Функция чтения заданных инпутов
    readInputs = (ids, keys) => {
        let input
        let inputs = new Map();
        let i = 0;

        for (let id of ids) {
            input = document.getElementById(id);
            if (input.value) {
                inputs.set(keys[i], input.value);
            }
            i++;
        }
        return inputs;
    }

    // Функция создания нового рейса
    addNewFlight = async ({
            flightID,
            destination,
            departureTimestamp,
            airplaneID,
        }) => {
        try {
            const addFlightResult = await AppModel.addFlight({
                flightID,
                departureTimestamp,
                destination,
                airplaneID
            });

            const airplaneName = document.getElementById(airplaneID).innerHTML;

            const newFlight = new Flight({
                flightID,
                destination,
                departureTimestamp,
                airplaneName,
            });
            this.#flights.push(newFlight);
            this.render();

            this.addNotification({ text: addFlightResult.message, type: 'success' });
            console.log(addFlightResult);
        } catch (err) {
            this.addNotification({ text: err.message, type: 'error' });
            console.error(err);
        }
    }

    // Функция изменения параметров существующего рейса
    editFlight = async ({
            flightID,
            destination,
            departureTimestamp,
            airplaneID,
        }) => {
        try {
            const editFlightResult = await AppModel.editFlight({
                flightID,
                destination,
                departureTimestamp,
                airplaneID
            });

            const flightObject = this.#flights.find((flight) => flight.flightID === flightID);

            let airplaneName = null;
            if (document.getElementById(airplaneID)) {
                airplaneName = document.getElementById(airplaneID).innerHTML;
            }

            if (destination) {
                flightObject.destination = destination;
            }
            if (departureTimestamp) {
                flightObject.departureTimestamp = departureTimestamp;
            }
            if (airplaneName) {
                flightObject.airplaneName = airplaneName;
            }

            this.render();

            this.addNotification({ text: editFlightResult.message, type: 'success' });
            console.log(editFlightResult);
        } catch (err) {
            this.addNotification({ text: err.message, type: 'error' });
            console.error(err);
        }
    }

    // Функция удаления существующего рейса
    deleteFlight = async ({ flightID }) => {
        try {
            const deleteFlightResult = await AppModel.deleteFlight({ flightID });

            this.#flights = this.#flights.filter((flight) => flight.flightID !== flightID);

            this.render();

            this.addNotification({ text: deleteFlightResult.message, type: 'success' });
            console.log(deleteFlightResult);
        } catch (err) {
            this.addNotification({ text: err.message, type: 'error' });
            console.error(err);
        }

    }

    // Функция добавления нового билета
    addNewTicket = async ({
            flightID,
            ticketID,
            fullName
        }) => {

        try {
            const addTicketResult = await AppModel.addTicket({
                ticketID,
                fullName,
                flightID
            });

            const newTicket = new Ticket({
                ticketID,
                fullName,
                flightID
            });

            this.#flights.find((flight) => flight.flightID === flightID).appendNewTicket(newTicket);

            this.render();

            this.addNotification({ text: addTicketResult.message, type: 'success' });
            console.log(addTicketResult);
        } catch (err) {
            this.addNotification({ text: err.message, type: 'error' });
            console.error(err);
        }
    }

    // Функция обновления параметров Билета
    editTicket = async ({
            flightID,
            ticketID,
            fullName
        }) => {

        try {
            const editTicketResult = await AppModel.editTicket({
                ticketID,
                fullName,
            });

            const flightObject = this.#flights.find((flight) => flight.flightID === flightID);
            const ticketObject = flightObject.tickets.find((ticket) => ticket.ticketID === ticketID);

            if (fullName) {
                ticketObject.fullName = fullName;
            }

            this.render();

            this.addNotification({ text: editTicketResult.message, type: 'success' });
            console.log(editTicketResult);
        } catch (err) {
            this.addNotification({ text: err.message, type: 'error' });
            console.error(err);
        }
    }

    // Функция удаления билета
    deleteTicket = async ({
        flightID,
        ticketID,
    }) => {

        try {
            const deleteTicketResult = await AppModel.deleteTicket({ ticketID });

            const flightObject = this.#flights.find((flight) => flight.flightID === flightID);

            flightObject.popTicket(ticketID);

            this.render();

            this.addNotification({ text: deleteTicketResult.message, type: 'success' });
            console.log(deleteTicketResult);
        } catch (err) {
            this.addNotification({ text: err.message, type: 'error' });
            console.error(err);
        }
    }

    // Функция переноса билета в другой рейс
    moveTicket = async ({
        srcFlightID,
        ticketID,
        destFlightID
    }) => {

        try {
            const moveTicketResult = await AppModel.moveTicket({
                srcFlightID,
                ticketID,
                destFlightID
            });

            const srcFlightObject = this.#flights.find((flight) => flight.flightID === srcFlightID);
            const ticketObject = srcFlightObject.tickets.find((ticket) => ticket.ticketID === ticketID);
            const destFlightObject = this.#flights.find((flight) => flight.flightID === destFlightID);

            srcFlightObject.popTicket(ticketID);
            ticketObject.flightID = destFlightID;
            destFlightObject.appendNewTicket(ticketObject);

            this.render();

            this.addNotification({ text: moveTicketResult.message, type: 'success' });
            console.log(moveTicketResult);
        } catch (err) {
            this.addNotification({ text: err.message, type: 'error' });
            console.error(err);
        }
    }

    render() {
        document.querySelector('.flights-list').remove();

        const ulElement = document.createElement('ul');
        ulElement.classList.add('flights-list', 'row', 'row-cols-4');

        const liElement = document.createElement('li');
        liElement.classList.add('col', 'flights-adder-li');

        const buttonElement = document.createElement('button');
        buttonElement.classList.add('flights-adder', 'btn', 'btn-outline-primary');
        buttonElement.setAttribute('type', 'button');
        buttonElement.innerHTML = '&#10010; Добавить рейс';
        buttonElement.addEventListener('click', () => { document.getElementById('modal-add-flight').showModal(); })

        liElement.appendChild(buttonElement);

        ulElement.appendChild(liElement);

        const mainElement = document.querySelector('.app-main');
        mainElement.appendChild(ulElement);

        for (let flight of this.#flights) {
            const flightElement = flight.render();
            const flightAdderElement = document.querySelector('.flights-adder-li');
            flightAdderElement.parentElement.insertBefore(flightElement, flightAdderElement);
        }
    }

    async init() {

        // Очистка форм диалогов и закрытие модального окна при нажатии кнопки Отмена
        // РЕЙСЫ
        document.getElementById('modal-add-flight').querySelector('.modal-cancel-btn')
            .addEventListener('click', () => {
                const ids = [
                    'modal-add-flight-input-id',
                    'modal-add-flight-input-destination',
                    'modal-add-flight-input-departure_timestamp',
                    'modal-add-flight-input-airplane_id',
                ];
                document.getElementById('modal-add-flight').close();
                this.clearInputs(ids);
            });
        document.getElementById('modal-edit-flight').querySelector('.modal-cancel-btn')
            .addEventListener('click', () => {
                const ids = [
                    'modal-edit-flight-input-destination',
                    'modal-edit-flight-input-departure_timestamp',
                    'modal-edit-flight-input-airplane_id',
                ];
                document.getElementById('modal-edit-flight').close();
                this.clearInputs(ids);
            });
        document.getElementById('modal-delete-flight').querySelector('.modal-cancel-btn')
            .addEventListener('click', () => {
                document.getElementById('modal-delete-flight').close();
            });
        // БИЛЕТЫ
        document.getElementById('modal-add-ticket').querySelector('.modal-cancel-btn')
            .addEventListener('click', () => {
                const ids = [
                    'modal-add-ticket-input-id',
                    'modal-add-ticket-input-full_name'
                ];
                document.getElementById('modal-add-ticket').close();
                this.clearInputs(ids);
            });
        document.getElementById('modal-edit-ticket').querySelector('.modal-cancel-btn')
            .addEventListener('click', () => {
                const ids = [
                    'modal-edit-ticket-input-full_name',
                ];
                document.getElementById('modal-edit-ticket').close();
                this.clearInputs(ids);
            });
        document.getElementById('modal-move-ticket').querySelector('.modal-cancel-btn')
            .addEventListener('click', () => {
                const ids = [
                    'modal-move-ticket-input-flight_id',
                ];
                document.getElementById('modal-move-ticket').close();
                this.clearInputs(ids);
            });
        document.getElementById('modal-delete-ticket').querySelector('.modal-cancel-btn')
            .addEventListener('click', () => {
                document.getElementById('modal-delete-ticket').close();
            });

        // Добавление обработчиков CRUD операций на кнопки соответствующие им кнопки ОК
        // РЕЙСЫ
        document.getElementById('modal-add-flight').querySelector('.modal-ok-btn')
            .addEventListener('click', () => {
                const ids = [
                    'modal-add-flight-input-id',
                    'modal-add-flight-input-destination',
                    'modal-add-flight-input-departure_timestamp',
                    'modal-add-flight-input-airplane_id',
                ];
                const keys = [
                    'flightID',
                    'destination',
                    'departureTimestamp',
                    'airplaneID',
                ];
                const inputs = this.readInputs(ids, keys);

                const {
                    flightID,
                    destination,
                    departureTimestamp,
                    airplaneID,
                } = {
                    flightID: inputs.get(keys[0]),
                    destination: inputs.get(keys[1]),
                    departureTimestamp: inputs.get(keys[2]),
                    airplaneID: inputs.get(keys[3])
                };

                this.clearInputs(ids);
                document.getElementById('modal-add-flight').close();

                this.addNewFlight({
                    flightID,
                    destination,
                    departureTimestamp,
                    airplaneID,
                });
            });
        document.getElementById('modal-edit-flight').querySelector('.modal-ok-btn')
            .addEventListener('click', (event) => {
                const flightID = event.target.getAttribute('flight_id');

                const ids = [
                    'modal-edit-flight-input-destination',
                    'modal-edit-flight-input-departure_timestamp',
                    'modal-edit-flight-input-airplane_id',
                ];
                const keys = [
                    'destination',
                    'departureTimestamp',
                    'airplaneID',
                ];
                const inputs = this.readInputs(ids, keys);

                const {
                    destination,
                    departureTimestamp,
                    airplaneID,
                } = {
                    destination: inputs.get(keys[0]),
                    departureTimestamp: inputs.get(keys[1]),
                    airplaneID: inputs.get(keys[2])
                };

                this.clearInputs(ids);
                document.getElementById('modal-edit-flight').close();

                this.editFlight({
                    flightID,
                    destination,
                    departureTimestamp,
                    airplaneID,
                });
            });
        document.getElementById('modal-delete-flight').querySelector('.modal-ok-btn')
            .addEventListener('click', (event) => {
                const flightID = event.target.getAttribute('flight_id');

                document.getElementById('modal-delete-flight').close();

                this.deleteFlight({
                    flightID
                });
            });
        // БИЛЕТЫ
        document.getElementById('modal-add-ticket').querySelector('.modal-ok-btn')
            .addEventListener('click', (event) => {
                const flightID = event.target.getAttribute('flight_id');

                const ids = [
                    'modal-add-ticket-input-id',
                    'modal-add-ticket-input-full_name',
                ];
                const keys = [
                    'ticketID',
                    'fullName',
                ];
                const inputs = this.readInputs(ids, keys);

                const {
                    ticketID,
                    fullName,
                } = {
                    ticketID: inputs.get(keys[0]),
                    fullName: inputs.get(keys[1]),
                };

                this.clearInputs(ids);
                document.getElementById('modal-add-ticket').close();

                this.addNewTicket({
                    flightID,
                    ticketID,
                    fullName
                });
            });
        document.getElementById('modal-edit-ticket').querySelector('.modal-ok-btn')
            .addEventListener('click', (event) => {
                const flightID = event.target.getAttribute('flight_id');
                const ticketID = event.target.getAttribute('ticket_id');

                const ids = [
                    'modal-edit-ticket-input-full_name',
                ];
                const keys = [
                    'fullName',
                ];
                const inputs = this.readInputs(ids, keys);

                const {
                    fullName
                } = {
                    fullName: inputs.get(keys[0]),
                };

                this.clearInputs(ids);
                document.getElementById('modal-edit-ticket').close();

                this.editTicket({
                    flightID,
                    ticketID,
                    fullName
                });
            });
        document.getElementById('modal-delete-ticket').querySelector('.modal-ok-btn')
            .addEventListener('click', (event) => {
                const flightID = event.target.getAttribute('flight_id');
                const ticketID = event.target.getAttribute('ticket_id');

                document.getElementById('modal-delete-ticket').close();

                this.deleteTicket({
                    flightID,
                    ticketID
                });
            });
        document.getElementById('modal-move-ticket').querySelector('.modal-ok-btn')
            .addEventListener('click', (event) => {
                const srcFlightID = event.target.getAttribute('flight_id');
                const ticketID = event.target.getAttribute('ticket_id');

                const ids = [
                    'modal-move-ticket-input-flight_id',
                ];
                const keys = [
                    'destFlightID',
                ];
                const inputs = this.readInputs(ids, keys);

                const {
                    destFlightID
                } = {
                    destFlightID: inputs.get(keys[0]),
                };

                this.clearInputs(ids);
                document.getElementById('modal-move-ticket').close();

                this.moveTicket({
                    srcFlightID,
                    ticketID,
                    destFlightID
                });
            });


        try {
            const {flights, airplanes} = await AppModel.getFlights();

            // Перенос серверной модели данных на клиент
            for (const flight of flights) {
                const flightObject = new Flight({
                    flightID: flight.flightID,
                    destination: flight.destination,
                    departureTimestamp: flight.departureTimestamp,
                    airplaneName: flight.airplane.name
                })

                for (const ticket of flight.tickets) {
                    const ticketObject = new Ticket({
                        ticketID: ticket.ticketID,
                        fullName: ticket.fullName,
                        flightID: flight.flightID
                    })

                    flightObject.appendNewTicket(ticketObject);
                }

                this.#flights.push(flightObject);
            }

            // Перенос доступных на выбор самолетов в теги select
            for (const airplane of airplanes) {
                const optionElem1 = document.createElement('option');
                optionElem1.setAttribute("value", airplane.airplaneID);
                optionElem1.setAttribute("id", airplane.airplaneID);
                optionElem1.innerHTML = airplane.name;
                document.getElementById('modal-add-flight-input-airplane_id').appendChild(optionElem1);

                const optionElem2 = document.createElement('option');
                optionElem2.setAttribute("value", airplane.airplaneID);
                optionElem2.innerHTML = airplane.name;
                document.getElementById('modal-edit-flight-input-airplane_id').appendChild(optionElem2);
            }

            this.render();
        } catch(err) {
            this.addNotification({ text: err.message, type: 'error' });
        }
    }

    addNotification = ({ text, type }) => {
        const notifications = document.getElementById('app-notifications');

        const notificationID = crypto.randomUUID();
        const notification = document.createElement('div');
        notification.classList.add(
            'notification',
            type === 'success' ? 'notification-success' : 'notification-error'
        );
        notification.setAttribute('id', notificationID);
        notification.innerHTML = text;

        notifications.appendChild(notification);

        notifications.show();

        setTimeout(() => {document.getElementById(notificationID).remove();}, 5000);
    }
};
