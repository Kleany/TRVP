import express from 'express';
import path from 'path';
import { fileURLToPath } from 'url';
import DB from './db/client.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// Считывание переменных окружения
const appHost = process.env.APP_BACKEND__HOST;
const appPort = Number(process.env.APP_BACKEND__PORT);

// Создание объекта приложения
const app = express();

// Создание объекта клиента для работы с БД
const db = new DB();

// MiddleWare для логирования потока запросов
app.use('*', (req, res, next) => {
    console.log(
        req.method,
        req.baseUrl || req.url,
        new Date().toISOString()
    );
    next();
});

// MiddleWare для разрешения пути к статическим файлам
app.use('/', express.static(path.resolve(__dirname, '../dist')));

//                                      REST ROUTES ДЛЯ РАБОТЫ РЕЙСАМИ

// GET Самолеты, рейсы и билеты
app.get('/flights', async (req, res) => {
    try {
        const [dbAirplanes, dbFlights, dbTickets] =
           await Promise.all([db.getAirplanes(), db.getFlights(), db.getTickets()]);

        const airplanes = dbAirplanes.map(({
            id,
            name,
            capacity
        }) => ({
            airplaneID: id,
            name,
            capacity
        }))

        const tickets = dbTickets.map(({
            id,
            full_name
        }) => ({
            ticketID: id,
            fullName: full_name
        }))

        const flights = dbFlights.map( flight => ({
            flightID: flight.id,
            departureTimestamp: flight.departure_timestamp,
            destination: flight.destination,
            airplane: airplanes.filter(airplane => airplane['airplaneID'] === flight.airplane_id)[0],
            tickets: tickets.filter(ticket => flight.tickets.indexOf(ticket.ticketID) !== -1)
        }))

        res.statusCode = 200;
        res.statusMessage = 'OK';
        res.json({ flights, airplanes });
    } catch (err) {
        res.statusCode = 500;
        res.statusMessage = 'Internal server error';
        res.json({
            timestamp: new Date().toISOString(),
            statusCode: 500,
            message: `Getting airplanes, flights and tickets error: ${err.error.message || err.error}`
        });
    }
});

// POST Создание рейса
app.use('/flights', express.json());
app.post('/flights', async (req, res) => {
    try {
        const {
            flightID,
            departureTimestamp,
            destination,
            airplaneID
        } = req.body;

        // Цепочка из проверки допустимости создания и создания
        await db.checkFlightByParams({ flightID, departureTimestamp, destination });
        await db.addFlight({ flightID, departureTimestamp, destination, airplaneID });

        res.statusCode = 200;
        res.statusMessage = 'OK';
        res.send();
    } catch(err) {
        switch (err.type) {
            case 'client':
                res.statusCode = 400;
                res.statusMessage = 'Bad request';
                break;

            default:
                res.statusCode = 500;
                res.statusMessage = 'Internal server error';
        }

        res.json({
            timestamp: new Date().toISOString(),
            statusCode: res.statusCode,
            message: `Create flight error: ${err.error.message || err.error}`
        });
    }
});

// PATCH Изменение параметров рейса
app.use('/flights/:flightID', express.json());
app.patch('/flights/:flightID', async (req, res) => {
    try {
        const { flightID } = req.params;
        const {
            departureTimestamp,
            destination,
            airplaneID
        } = req.body;

        // Цепочка из проверки допустимости изменения и изменения
        await db.checkFlightByParams({ flightID, departureTimestamp, destination });
        await db.updateFlight({ flightID, departureTimestamp, destination, airplaneID });

        res.statusCode = 200;
        res.statusMessage = 'OK';
        res.send();
    } catch(err) {
        switch (err.type) {
            case 'client':
                res.statusCode = 400;
                res.statusMessage = 'Bad request';
                break;

            default:
                res.statusCode = 500;
                res.statusMessage = 'Internal server error';
        }

        res.json({
            timestamp: new Date().toISOString(),
            statusCode: res.statusCode,
            message: `Update flight error: ${err.error.message || err.error}`
        });
    }
});

// DELETE Удаление рейса
app.delete('/flights/:flightID', async (req, res) => {
    try {
        const { flightID } = req.params;

        // Цепочка проверки допустимости удаления и удаления
        await db.checkIfFlightIsEmpty({ flightID });
        await db.deleteFlight({ flightID });

        res.statusCode = 200;
        res.statusMessage = 'OK';
        res.send();
    } catch(err) {
        switch (err.type) {
            case 'client':
                res.statusCode = 400;
                res.statusMessage = 'Bad request';
                break;

            default:
                res.statusCode = 500;
                res.statusMessage = 'Internal server error';
        }

        res.json({
            timestamp: new Date().toISOString(),
            statusCode: res.statusCode,
            message: `Delete flight error: ${err.error.message || err.error}`
        });
    }
});

//                                      REST ROUTES ДЛЯ РАБОТЫ БИЛЕТАМИ

// POST Создание билета
app.use('/tickets', express.json());
app.post('/tickets', async (req, res) => {
    try {
        const {
            ticketID,
            fullName,
            flightID
        } = req.body;

        // Цепочка из проверки допустимости создания и создания
        await db.checkTicketByFullName({ fullName });
        await db.checkFlightCapacity({ flightID });
        await db.addTicket({ ticketID, fullName, flightID });

        res.statusCode = 200;
        res.statusMessage = 'OK';
        res.send();
    } catch(err) {
        switch (err.type) {
            case 'client':
                res.statusCode = 400;
                res.statusMessage = 'Bad request';
                break;

            default:
                res.statusCode = 500;
                res.statusMessage = 'Internal server error';
        }

        res.json({
            timestamp: new Date().toISOString(),
            statusCode: res.statusCode,
            message: `Create ticket error: ${err.error.message || err.error}`
        });
    }
});

// PATCH Изменение параметров билета
app.use('/tickets/:ticketID', express.json());
app.patch('/tickets/:ticketID', async (req, res) => {
    try {
        const { ticketID } = req.params;
        const { fullName } = req.body;

        // Цепочка из проверки допустимости изменения и изменения
        await db.checkTicketByFullName({ fullName });
        await db.updateTicket({ ticketID, fullName });

        res.statusCode = 200;
        res.statusMessage = 'OK';
        res.send();
    } catch(err) {
        switch (err.type) {
            case 'client':
                res.statusCode = 400;
                res.statusMessage = 'Bad request';
                break;

            default:
                res.statusCode = 500;
                res.statusMessage = 'Internal server error';
        }

        res.json({
            timestamp: new Date().toISOString(),
            statusCode: res.statusCode,
            message: `Update ticket params error: ${err.error.message || err.error}`
        });
    }
});

// DELETE Удаление билета
app.delete('/tickets/:ticketID', async (req, res) => {
    try {
        const { ticketID } = req.params;
        await db.deleteTicket({ ticketID });

        res.statusCode = 200;
        res.statusMessage = 'OK';
        res.send();
    } catch(err) {
        switch (err.type) {
            case 'client':
                res.statusCode = 400;
                res.statusMessage = 'Bad request';
                break;

            default:
                res.statusCode = 500;
                res.statusMessage = 'Internal server error';
        }

        res.json({
            timestamp: new Date().toISOString(),
            statusCode: res.statusCode,
            message: `Delete ticket params error: ${err.error.message || err.error}`
        });
    }
});

// PATCH Перенос билета из одного рейса в другой
app.patch('/flights', async (req, res) => {
    try {
        const { ticketID, srcFlightID, destFlightID } = req.body;

        // Цепочка из проверки допустимости переноса и переноса
        await db.checkFlightCapacity({ flightID: destFlightID });
        await db.checkFlightDestination({ srcFlightID, destFlightID });
        await db.moveTicket({ ticketID, srcFlightID, destFlightID });

        res.statusCode = 200;
        res.statusMessage = 'OK';
        res.send();
    } catch(err) {
        switch (err.type) {
            case 'client':
                res.statusCode = 400;
                res.statusMessage = 'Bad request';
                break;

            default:
                res.statusCode = 500;
                res.statusMessage = 'Internal server error';
        }

        res.json({
            timestamp: new Date().toISOString(),
            statusCode: res.statusCode,
            message: `Move ticket params error: ${err.error.message || err.error}`
        });
    }
});

app.listen(appPort, appHost, async () => {
    // Инициализация подключения к БД
    try {
        await db.connect();
    } catch (error) {
        console.log('Rick Roll Airlines app shut down');
        process.exit(100);
    }

    console.log(`Rick Roll Airlines manager app started at host http://${appHost}:${appPort}`);
});

// Установка обработчика закрытия сервера при появлении сигнала ошибки SIGTERM
process.on('SIGTERM', () => {
   console.log('SIGTERM signal received: closing HTTP server..');
   server.close(async  () => {
       await db.disconnect();
       console.log('HTTP server was closed');
   });
});