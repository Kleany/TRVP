import pg from 'pg';

// Класс клиента для работы с БД
export default class DB {
    #dbClient = null;
    #dbHost = '';
    #dbPort = '';
    #dbName = '';
    #dbLogin = '';
    #dbPassword = '';

    constructor() {
        this.#dbHost = process.env.DB__HOST;
        this.#dbPort = process.env.DB__PORT;
        this.#dbName = process.env.DB__DATABASE;
        this.#dbLogin = process.env.DB__USER;
        this.#dbPassword = process.env.DB__PASSWORD;

        this.#dbClient = new pg.Client({
            user: this.#dbLogin,
            password: this.#dbPassword,
            host: this.#dbHost,
            port: this.#dbPort,
            database: this.#dbName
        });
    }

    // Метод подключения к БД
    async connect() {
        try {
            await this.#dbClient.connect();
            console.log('DB connection established')
        } catch (error) {
            console.error('Unable to connect to DB: ', error)
        }
    }

    // Метод отключения от БД
    async disconnect() {
        await this.#dbClient.end();
        console.log('DB connection was closed')
    }

    // МЕТОДЫ ПОЛУЧЕНИЯ ВСЕХ САМОЛЕТОВ, РЕЙСОВ И БИЛЕТОВ

    // Метод получения всех самолетов
    async getAirplanes() {
        try {
            const airplanes = await this.#dbClient.query(
                'SELECT * FROM airplanes;'
            );
            return airplanes.rows;
        } catch (error) {
            console.error('Unable to get airplanes. Error: ', error);
            return Promise.reject({
                type: 'internal',
                error
            });
        }
    }

    // Метод получения всех рейсов
    async getFlights() {
        try {
            const flights = await this.#dbClient.query(
                'SELECT * FROM flights;'
            );
            return flights.rows;
        } catch (error) {
            console.error('Unable to get flights. Error: ', error);
            return Promise.reject({
                type: 'internal',
                error
            });
        }
    }

    // Метод получения всех билетов
    async getTickets() {
        try {
            const tickets = await this.#dbClient.query(
                'SELECT * FROM tickets;'
            );
            return tickets.rows;
        } catch (error) {
            console.error('Unable to get tickets. Error: ', error);
            return Promise.reject({
                type: 'internal',
                error
            });
        }
    }

    // МЕТОДЫ ДЛЯ ОПЕРИРОВАНИЯ С РЕЙСАМИ

    // Метод создания нового рейса
    async addFlight({
        flightID,
        departureTimestamp,
        destination,
        airplaneID
    } = {
        flightID: null,
        departureTimestamp: '',
        destination: '',
        airplaneID: null
    }) {
        if (!flightID || !departureTimestamp || !destination || !airplaneID) {
            const errMsg = `Add flight error: wrong params (flightID: ${flightID}, departureTimestamp: ${departureTimestamp}, destination: ${destination}, airplaneID: ${airplaneID})`;
            console.error(errMsg);
            return Promise.reject({
                type: 'client',
                error: new Error(errMsg)
            });
        }

        try {
            await this.#dbClient.query(
                'INSERT INTO flights (id, departure_timestamp, destination, airplane_id) VALUES ($1, $2, $3, $4);',
                [flightID, departureTimestamp, destination, airplaneID]
            );
        } catch (error) {
            console.error('Unable to create flight. Error: ', error);
            return Promise.reject({
                type: 'internal',
                error
            });
        }
    }

    // Метод для изменения параметров рейса
    async updateFlight({
        flightID,
        departureTimestamp,
        destination,
        airplaneID
    } = {
        flightID: null,
        departureTimestamp: null,
        destination: '',
        airplaneID: null
    }) {
        if (!flightID || (!departureTimestamp && !destination && !airplaneID) ) {
            const errMsg = `Update flight error: wrong params (flightID: ${flightID}, departureTimestamp: ${departureTimestamp}, destination: ${destination}, airplaneID: ${airplaneID})`;
            console.error(errMsg);
            return Promise.reject({
                type: 'client',
                error: new Error(errMsg)
            });
        }

        try {
            if (departureTimestamp) {
                await this.#dbClient.query(
                    'UPDATE flights SET departure_timestamp = $2 WHERE id = $1;',
                    [flightID, departureTimestamp]
                );
            }
            if (destination) {
                await this.#dbClient.query(
                    'UPDATE flights SET destination = $2 WHERE id = $1;',
                    [flightID, destination]
                );
            }
            if (airplaneID) {
                await this.#dbClient.query(
                    'UPDATE flights SET airplane_id = $2 WHERE id = $1;',
                    [flightID, airplaneID]
                );
            }
        } catch (error) {
            console.error('Unable to update flight. Error: ', error);
            return Promise.reject({
                type: 'internal',
                error
            });
        }
    }

    // Метод для удаления существующего рейса
    async deleteFlight ({ flightID } = { flightID: null }) {
        if (!flightID ) {
            const errMsg = `Delete flight error: wrong params (flightID: ${flightID})`;
            console.error(errMsg);
            return Promise.reject({
                type: 'client',
                error: new Error(errMsg)
            });
        }

        try {
            await this.#dbClient.query(
                'DELETE FROM flights WHERE id = $1;',
                [flightID]
            );
        } catch (error) {
            console.error('Unable to delete flight. Error: ', error);
            return Promise.reject({
                type: 'internal',
                error
            });
        }
    }

    // МЕТОДЫ ДЛЯ ОПЕРИРОВАНИЯ С БИЛЕТАМИ

    // Метод создания нового билета
    async addTicket({
        ticketID,
        fullName,
        flightID
    } = {
        ticketID: null,
        fullName: '',
        flightID: null
    }) {
        if (!ticketID || !fullName || !flightID ) {
            const errMsg = `Create ticket error: wrong params (ticketID: ${ticketID}, fullName: ${fullName}, flightID: ${flightID})`;
            console.error(errMsg);
            return Promise.reject({
                type: 'client',
                error: new Error(errMsg)
            });
        }

        try {
            await this.#dbClient.query(
                'INSERT INTO tickets (id, full_name, flight_id) VALUES ($1, $2, $3);',
                [ticketID, fullName, flightID]
            );
            await this.#dbClient.query(
                'UPDATE flights SET tickets = array_append(tickets, $1) WHERE id = $2;',
                [ticketID, flightID]
            );
        } catch (error) {
            console.error('Unable to create ticket. Error: ', error);
            return Promise.reject({
                type: 'internal',
                error
            });
        }
    }

    // Метод для изменения параметров бронирования (ФИО)
    async updateTicket({ ticketID, fullName } = { ticketID: null, fullName: '' }) {
        if (!ticketID || !fullName ) {
            const errMsg = `Update ticket error: wrong params
            (flightID: ${ticketID}, fullName: ${fullName})`;
            console.error(errMsg);
            return Promise.reject({
                type: 'client',
                error: new Error(errMsg)
            });
        }

        try {
            await this.#dbClient.query(
                'UPDATE tickets SET full_name = $2 WHERE id = $1;',
                [ticketID, fullName]
            );
        } catch (error) {
            console.error('Unable to update ticket. Error: ', error);
            return Promise.reject({
                type: 'internal',
                error
            });
        }
    }

    // Метод для переноса бронирования из одного рейса в другой
    async moveTicket({
        ticketID,
        srcFlightID,
        destFlightID
    } = {
        ticketID: null,
        srcFlightID: null,
        destFlightID: null
    }) {
        if (!ticketID || !srcFlightID || !destFlightID) {
            const errMsg = `Move ticket error: wrong params (flightID: ${ticketID}, srcFlightID: ${srcFlightID}, destFlightID: ${destFlightID})`;
            console.error(errMsg);
            return Promise.reject({
                type: 'client',
                error: new Error(errMsg)
            });
        }

        try {
            await this.#dbClient.query(
                'UPDATE tickets SET flight_id = $2 WHERE id = $1;',
                [ticketID, destFlightID]
            );
            await this.#dbClient.query(
                'UPDATE flights SET tickets = array_append(tickets, $1) WHERE id = $2;',
                [ticketID, destFlightID]
            );
            await this.#dbClient.query(
                'UPDATE flights SET tickets = array_remove(tickets, $1) WHERE id = $2;',
                [ticketID, srcFlightID]
            );
        } catch (error) {
            console.error('Unable to move ticket. Error: ', error);
            return Promise.reject({
                type: 'internal',
                error
            });
        }
    }

    // Метод для удаления бронирования
    async deleteTicket ({ ticketID } = { ticketID: null }) {
        if (!ticketID ) {
            const errMsg = `Delete ticket error: wrong params (ticketID: ${ticketID})`;
            console.error(errMsg);
            return Promise.reject({
                type: 'client',
                error: new Error(errMsg)
            });
        }

        try {
            const queryResult = await this.#dbClient.query(
                'SELECT flight_id FROM tickets WHERE id = $1;',
                [ticketID]
            );

            const { flight_id: flightID } = queryResult.rows[0];

            await this.#dbClient.query(
                'DELETE FROM tickets WHERE id = $1;',
                [ticketID]
            );

            await this.#dbClient.query(
                'UPDATE flights SET tickets = array_remove(tickets, $1) WHERE id = $2;',
                [ticketID, flightID]
            );

        } catch (error) {
            console.error('Unable to delete ticket. Error: ', error);
            return Promise.reject({
                type: 'internal',
                error
            });
        }
    }

    // ДОПОЛНИТЕЛЬНЫЕ МЕТОДЫ ДЛЯ ПРОВЕРОК

    // Метод проверки, пустой ли рейс (для для удаления рейса)
    async checkIfFlightIsEmpty({ flightID } = { flightID: null }) {
        if (!flightID ) {
            const errMsg = `Check if flight is empty error: wrong params (flightID: ${flightID})`;
            console.error(errMsg);
            return Promise.reject({
                type: 'client',
                error: new Error(errMsg)
            });
        }

        try {
            const queryResult1 = await this.#dbClient.query(
                'SELECT tickets, airplane_id FROM flights WHERE id = $1;',
                [flightID]
            );
            const currentTicketCount = queryResult1.rows[0]['tickets'].length;

            if (currentTicketCount > 0) {
                const errMsg = `Flight ${flightID} is not empty`;
                return Promise.reject({
                    type: 'client',
                    error: new Error(errMsg)
                });
            }
        } catch (error) {
            console.error('Unable to check if flight is empty. Error: ', error);
            return Promise.reject({
                type: 'internal',
                error
            });
        }
    }

    // Метод для проверки существования рейса с заданными временем отправлением и пунктом назначения (для создания и обновлнеия рейса)
    async checkFlightByParams({ flightID, departureTimestamp, destination } = { flightID: null, departureTimestamp: null, destination: '' }) {
        if (!flightID || (!departureTimestamp && !destination)) {
            const errMsg = `Check flight by params error: wrong params (flightID: ${flightID}, departureTimestamp: ${departureTimestamp}, destination: ${destination})`;
            console.error(errMsg);
            return Promise.reject({
                type: 'client',
                error: new Error(errMsg)
            });
        }

        try {
            let newDepartureTimestamp;
            let newDestination;
            if (!departureTimestamp) {
                const preQueryResult = await this.#dbClient.query(
                    'SELECT departure_timestamp FROM flights WHERE id = $1;',
                    [flightID]
                );
                newDepartureTimestamp = preQueryResult.rows[0] ? preQueryResult.rows[0]['departure_timestamp'] : null;
            }
            else {
                newDepartureTimestamp = departureTimestamp;
            }
            if (!destination) {
                const preQueryResult = await this.#dbClient.query(
                    'SELECT destination FROM flights WHERE id = $1;',
                    [flightID]
                );
                newDestination = preQueryResult.rows[0] ? preQueryResult.rows[0]['destination'] : '';
            }
            else {
                newDestination = destination;
            }

            const queryResult = await this.#dbClient.query(
                'SELECT id FROM flights WHERE departure_timestamp = $1 AND destination = $2;',
                [newDepartureTimestamp, newDestination]
            );
            const existingFlightID = queryResult.rows[0];

            if (existingFlightID) {
                const errMsg = `Flight ${existingFlightID['id']} with departureTimestamp ${departureTimestamp} and destination ${destination} already exists`;
                return Promise.reject({
                    type: 'client',
                    error: new Error(errMsg)
                });
            }
        } catch (error) {
            console.error('Unable to check flight by params. Error: ', error);
            return Promise.reject({
                type: 'internal',
                error
            });
        }
    }

    // Метод для проверки наличия свободного места в рейсе (для создания и переноса билета)
    async checkFlightCapacity({ flightID } = { flightID: null }) {
        if (!flightID) {
            const errMsg = `Check flight capacity tickets error: wrong params (flightID: ${flightID})`;
            console.error(errMsg);
            return Promise.reject({
                type: 'client',
                error: new Error(errMsg)
            });
        }

        try {
            const queryResult1 = await this.#dbClient.query(
                'SELECT tickets, airplane_id FROM flights WHERE id = $1;',
                [flightID]
            );
            const currentTicketCount = queryResult1.rows[0]['tickets'].length;
            const airplaneID = queryResult1.rows[0]['airplane_id'];

            const queryResult2 = await this.#dbClient.query(
                'SELECT capacity FROM airplanes WHERE id = $1;',
                [airplaneID]
            );
            const maximumCapacity = queryResult2.rows[0]['capacity'];

            if (currentTicketCount >= maximumCapacity) {
                const errMsg = `Flight ${flightID} has no seats left`;
                return Promise.reject({
                    type: 'client',
                    error: new Error(errMsg)
                });
            }
        } catch (error) {
            console.error('Unable to check flight capacity. Error: ', error);
            return Promise.reject({
                type: 'internal',
                error
            });
        }
    }

    // Метод для проверки пункта назначения целевого рейса (для переноса билета)
    async checkFlightDestination({ srcFlightID, destFlightID } = { srcFlightID: null, destFlightID: null }) {
        if (!srcFlightID || !destFlightID) {
            const errMsg = `Check flight destination error: wrong params (srcFlightID: ${srcFlightID}, destFlightID: ${destFlightID})`;
            console.error(errMsg);
            return Promise.reject({
                type: 'client',
                error: new Error(errMsg)
            });
        }

        try {
            const queryResult1 = await this.#dbClient.query(
                'SELECT destination FROM flights WHERE id = $1;',
                [srcFlightID]
            );
            const queryResult2 = await this.#dbClient.query(
                'SELECT destination FROM flights WHERE id = $1;',
                [destFlightID]
            );
            const srcDestination = queryResult1.rows[0]['destination'];
            const destDestination = queryResult2.rows[0]['destination'];

            if (srcDestination !== destDestination) {
                const errMsg = `Flight ${destFlightID} has different destination`;
                return Promise.reject({
                    type: 'client',
                    error: new Error(errMsg)
                });
            }
        } catch (error) {
            console.error('Unable to check flight destination. Error: ', error);
            return Promise.reject({
                type: 'internal',
                error
            });
        }
    }

    // Метод для проверки существования бронирования с заданным ФИО (для создания и обновлнеия билета)
    async checkTicketByFullName({ fullName } = { fullName: '' }) {
        if (!fullName) {
            const errMsg = `Check ticket by full name error: wrong params (fullName: ${fullName})`;
            console.error(errMsg);
            return Promise.reject({
                type: 'client',
                error: new Error(errMsg)
            });
        }

        try {
            const queryResult = await this.#dbClient.query(
                'SELECT id from tickets WHERE full_name = $1;',
                [fullName]
            );
            const existingTicketID = queryResult.rows[0];

            if (existingTicketID) {
                const errMsg = `Ticket ${existingTicketID['id']} for client ${fullName} already exists`;
                return Promise.reject({
                    type: 'client',
                    error: new Error(errMsg)
                });
            }
        } catch (error) {
            console.error('Unable to check ticket by full name. Error: ', error);
            return Promise.reject({
                type: 'internal',
                error
            });
        }
    }
};