------ I. Скрипты для создания таблиц

-- Таблица "самолеты" (ID - UUID)
CREATE TABLE IF NOT EXISTS airplanes (
    id UUID PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    capacity INTEGER NOT NULL CHECK ( capacity > 0 )
);

-- Таблица "рейсы" (ID - "RickRoll0000")
CREATE TABLE IF NOT EXISTS flights (
    id VARCHAR(100) PRIMARY KEY,
    departure_timestamp TIMESTAMP NOT NULL,
    destination VARCHAR(100) NOT NULL,
    airplane_id UUID NOT NULL REFERENCES airplanes,
    tickets VARCHAR[6][] DEFAULT '{}'
);

-- Таблица "бронирования" (ID - "M1N4YA")
CREATE TABLE IF NOT EXISTS tickets (
    id        VARCHAR(6) PRIMARY KEY,
    full_name VARCHAR(100) NOT NULL,
    flight_id VARCHAR(100) NOT NULL REFERENCES flights
);

------ II. Запросы для работы с таблицами

---- 1) Запросы для таблицы "самолеты"

-- Занесение новых самолетов (делается заблаговременно)
INSERT INTO airplanes (id, name, capacity) VALUES ('6c409092-b7ec-11ee-8690-d79028b61534', 'Airbus 321NEO', 5);
INSERT INTO airplanes (id, name, capacity) VALUES ('3eb74e80-b7ed-11ee-84f0-4be27053734a', 'Boeing 737 MAX 8', 3);
INSERT INTO airplanes (id, name, capacity) VALUES ('bcef17d4-b7ec-11ee-bbd8-dbbaae56a785', 'Embraer ERJ-170', 2);

-- Получение списка имеющихся самолетов !!!DONE
SELECT * FROM airplanes;

-- Получение вместимости заданного самолета !!!DONE
SELECT capacity FROM airplanes WHERE id = '6c409092-b7ec-11ee-8690-d79028b61534';

---- 2) Запросы для таблицы "рейсы"

-- Получение списка имеющихся рейсов !!!DONE
SELECT * FROM flights;

-- Получение списка бронирований на заданный рейс, a также ID используемого самолета !!!DONE
SELECT tickets, airplane_id FROM flights WHERE id = 'RickRoll3017';

-- Получение времени отправления заданного рейса (если изменяется только пункт назначения) !!!DONE
SELECT destination FROM flights WHERE id = 'RickRoll3017';
-- Получение пункта назначения заданного рейса (если изменяется только время отправления) !!!DONE
SELECT departure_timestamp FROM flights WHERE id = 'RickRoll3017';
-- Получение ID рейса с заданными временем отправлением и пунктом назначения (если такой существует) !!!DONE
SELECT id FROM flights WHERE departure_timestamp = '2022-12-28 21:30:00' AND destination = 'Иркутск';

-- Получение пункта назначения заданного рейса !!!DONE
SELECT destination FROM flights WHERE id = 'RickRoll3017';

-- Создание нового рейса !!!DONE
INSERT INTO flights (id, departure_timestamp, destination, airplane_id)
VALUES ('RickRoll3017', '2022-12-28 21:30:00', 'Иркутск', '6c409092-b7ec-11ee-8690-d79028b61534');

-- Изменение времени отправления рейса !!!DONE
UPDATE flights SET departure_timestamp = '2022-12-28 21:30:00' WHERE id = 'RickRoll3017';
-- Изменение пункта назначения рейса !!!DONE
UPDATE flights SET destination = 'Иркутск' WHERE id = 'RickRoll3017';
-- Изменение задействованного в рейсе самолета !!!DONE
UPDATE flights SET airplane_id = '6c409092-b7ec-11ee-8690-d79028b61534' WHERE id = 'RickRoll3017';

-- Удаление существущего ПУСТОГО рейса !!!DONE
DELETE FROM flights WHERE id = 'RickRoll3017';

---- 3) Запросы для таблицы "билеты"

-- Получения списка существущих бронирований !!!DONE
SELECT * from tickets;

-- Получения ID бронирования для заданного бронирующего (ФИО), если такое существует !!!DONE
SELECT id from tickets WHERE full_name = 'Макаренко Никита Андреевич';

-- Создание нового билета !!! DONE
INSERT INTO tickets (id, full_name, flight_id) VALUES ('M1N4YA', 'Макаренко Никита Андреевич', 'RickRoll3017');
UPDATE flights SET tickets = array_append(tickets, 'M1N4YA') WHERE id = 'RickRoll3017';

-- Перетаскивание билета с одного рейса на другой !!!DONE
UPDATE tickets SET flight_id = 'RickRoll3017' WHERE id = 'M1N4YA';
UPDATE flights SET tickets = array_append(tickets, 'M1N4YA') WHERE id = 'RickRoll3017';
UPDATE flights SET tickets = array_remove(tickets, 'M1N4YA') WHERE id = 'RickRoll2018';

-- Изменение ФИО бронирующего !!!DONE
UPDATE tickets SET full_name = 'Макаренко Никита Андреевич' WHERE id = 'M1N4YA';

-- Удаление билета !!!DONE
SELECT flight_id FROM tickets WHERE id = 'M1N4YA';
DELETE FROM tickets WHERE id = 'M1N4YA';
UPDATE flights SET tickets = array_remove(tickets, 'M1N4YA') WHERE id = 'RickRoll2018';
