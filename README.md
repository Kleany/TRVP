# Макаренко Н.А. РК6-74Б

Домашнее задание по курсу "Технологии разработки веб-приложений"

Вариант - ТРВП-016

# Подготовка

Необходимо задать переменные окружения в файле `.env` для базы данных и бэкенда.
Далее необходимо создать таблицы в базе данных (см. файл `sql_scripts`) и занести в таблицу
airplanes несколько самолетов.

# Сборка

Необходим установленный Docker и Docker Compose

```bash
docker compose up
```

# Подключение

Приложение доступно по адрессу `https://localhost:${APP_BACKEND__PORT}`

# Скринкаст

Доступен по ссылке https://disk.yandex.ru/i/DcgH38dJ8DahKQ

# Контакты

Макаренко Никита РК6-74Б

Samoware - makarenkona@student.bmstu.ru

GMail - maknik1305@gmail.com

Telegram - https://t.me/Kleanie

VK - https://vk.com/kleany

